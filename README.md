# Solution for the backend developer test
The solution is a Spring Boot Web Service application. It has endpoints exposing the following operations:
 - User adding
 - Retrieve User by Id
 - Add a user location

# Building and Running
## Pre-requisites
- Java 1.8
- Maven 3.2.5 or above

# How to run
To build the application:

`mvn clean install`

To launch the Spring Boot application:

`mvn clean install spring-boot:run -Dspring.profiles.active=local,h2`

Then open navigate to [page](http://localhost:8080/swagger-ui.html)

This will launch the page with the Swagger API that could be used to test the endpoints.

