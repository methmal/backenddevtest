package uk.co.snatch.devtest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import springfox.documentation.annotations.ApiIgnore;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


@EnableSwagger2
@SpringBootApplication(scanBasePackages = "uk.co.snatch")
public class BackendDevTestApplication {

    @Value("${swagger.api.version}")
    String swaggerApiVersion;

    @Bean
    public Docket swaggerApiDocumentation(){
        return new Docket(DocumentationType.SWAGGER_2).useDefaultResponseMessages(false)
                .groupName("Api")
                .ignoredParameterTypes(ApiIgnore.class)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("uk.co.snatch"))
                .build()
                .pathMapping("/");
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("User service API Documentation")
                .description("API Documentation")
                .version(swaggerApiVersion)
                .build();
    }

    public static void main(String[] args) {
        SpringApplication.run(BackendDevTestApplication.class, args);

    }


}
