package uk.co.snatch.devtest.controller;

import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uk.co.snatch.devtest.model.ErrorResponse;
import uk.co.snatch.devtest.model.LocationAddResponse;
import uk.co.snatch.devtest.model.User;
import uk.co.snatch.devtest.model.UserAddResponse;
import uk.co.snatch.devtest.service.UserService;
import uk.co.snatch.devtest.util.UsernameValidator;

import javax.validation.Valid;

@RestController
@RequestMapping("/user")
@Api(tags = "User Controller", description = "User related operations")
public class UserController {

    private static final Logger LOG = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    @Autowired
    private UsernameValidator validator;


    @ApiOperation(value = "Add a User", nickname = "addUser",
            notes = "This endpoint provides the functionality to create a new user")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "All Good!", response = UserAddResponse.class),
            @ApiResponse(code = 400, message = "Error Occurred",
                    response = ErrorResponse.class),
            @ApiResponse(code = 500, message = "Server Error",
                    response = ErrorResponse.class)})
    @RequestMapping(value = "/add", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity createUser(@ApiParam(required = true, name = "payload", value = "The Customer JSON payload") @Valid @RequestBody User payload ) {
        LOG.debug("Adding user "+payload.getUsername());

        if (validator.isValidUsername(payload.getUsername())) {

            UserAddResponse userAddResponse = userService.createUser(payload);

            return new ResponseEntity<UserAddResponse>(userAddResponse, HttpStatus.OK);
        } else {
            return new ResponseEntity<ErrorResponse>(new ErrorResponse("001", "Invalid username"), HttpStatus.BAD_REQUEST);
        }

    }

    @ApiOperation(value = "Retrieve User details", notes = "Retrieve User details")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = User.class),
            @ApiResponse(code = 400, message = "Error Occurred ", response = ErrorResponse.class),
            @ApiResponse(code = 500, message = "Internal Server Error", response = ErrorResponse.class)})
    @ApiImplicitParams({
            @ApiImplicitParam(name = "user-id", value = "User Id",
                    required = true, dataType = "string", paramType = "path", defaultValue="1")
    })
    @RequestMapping(value = "/{user-id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public User retrieveUserDetails(@PathVariable("user-id") long userId) {

        return userService.getUser(userId);
    }

    @ApiOperation(value = "Add User location details", notes = "Add User location details")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = LocationAddResponse.class),
            @ApiResponse(code = 400, message = "Error Occurred ", response = ErrorResponse.class),
            @ApiResponse(code = 500, message = "Internal Server Error", response = ErrorResponse.class)})
    @ApiImplicitParams({
            @ApiImplicitParam(name = "user-id", value = "User Id", required = true, dataType = "string",
                    paramType = "path", defaultValue="1"),
            @ApiImplicitParam(name = "latitude", value = "Latitude", required = true, dataType = "string",
                    paramType = "path", defaultValue="-15.623037"),
            @ApiImplicitParam(name = "longitude", value = "Longitude", required = true,
                    dataType = "string", paramType = "path", defaultValue="18.388672"),
    })
    @RequestMapping(value = "/{user-id}/{latitude}/{longitude}",
            method = {RequestMethod.PUT},
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public LocationAddResponse addUserLocation(@PathVariable("user-id") long userId, @PathVariable("latitude") String latitude,
                                                 @PathVariable("longitude") String longitude
                                                 ) {
        return userService.addLocation(userId, latitude, longitude);
    }

}
