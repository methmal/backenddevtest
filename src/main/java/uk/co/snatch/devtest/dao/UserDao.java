package uk.co.snatch.devtest.dao;

import uk.co.snatch.devtest.model.LocationAddResponse;
import uk.co.snatch.devtest.model.User;
import uk.co.snatch.devtest.model.UserAddResponse;

public interface UserDao {

    User findById(long id);

    LocationAddResponse addLocation(long userId, String longitude, String latitude);

    UserAddResponse addUser(User user);
}
