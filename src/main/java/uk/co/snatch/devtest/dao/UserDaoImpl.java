package uk.co.snatch.devtest.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import uk.co.snatch.devtest.model.LocationAddResponse;
import uk.co.snatch.devtest.model.User;
import uk.co.snatch.devtest.model.UserAddResponse;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@Repository
public class UserDaoImpl implements UserDao {

    private static final Logger LOG = LoggerFactory.getLogger(UserDaoImpl.class);

    private static final String GOOGLE_MAP_LINK = "https://www.google.com/maps/?q=";

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    private static final String SELECT_USER_BY_ID_SQL = "SELECT * FROM users WHERE id=:id";
    private static final String UPDATE_USER_LOCATION_SQL =
            "UPDATE users SET latitude=:latitude,longitude=:longitude,link=:link WHERE id=:id";
    private static final String ADD_USER_SQL = "INSERT into users (username,email,phone," +
            "latitude,longitude,link) VALUES (:username,:email,:phone,:latitude,:longitude,:link)";
    
    public void setNamedParameterJdbcTemplate(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    public NamedParameterJdbcTemplate getNamedParameterJdbcTemplate(){return this.namedParameterJdbcTemplate;};

    public User findById(long id) {

        User result = null;

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("id", id);

        try {
            result = getNamedParameterJdbcTemplate().queryForObject(SELECT_USER_BY_ID_SQL, params, new UserMapper());
        }catch (Exception sqe){
            LOG.error("Error getting user:"+id, sqe);
        }
        return result;
    }

    public LocationAddResponse addLocation(long id, String longitude, String latitude) {

        LocationAddResponse response = new LocationAddResponse();

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("id", id);
        params.put("latitude", latitude);
        params.put("longitude", longitude);
        params.put("link", GOOGLE_MAP_LINK+latitude+","+longitude);

        try {
            int result = getNamedParameterJdbcTemplate().update(UPDATE_USER_LOCATION_SQL, params);

            if (result > 0) {
                response.setSuccess(true);
            } else {
                response.setSuccess(false);
            }
        } catch (Exception e){
            LOG.error("Error adding location for user:"+id, e);
        }

        return response;
    }

    public UserAddResponse addUser(User user) {

        UserAddResponse response = new UserAddResponse();

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("username", user.getUsername());
        params.put("email", user.getEmailAddress());
        params.put("phone", user.getPhoneNumber());
        params.put("latitude", user.getLatitude());
        params.put("longitude",user.getLongitude());
        params.put("link", GOOGLE_MAP_LINK+user.getLatitude()+","+user.getLongitude());

        try {
            int result = getNamedParameterJdbcTemplate().update(ADD_USER_SQL, params);
            //@TODO Return the id of the inserted row

            if (result > 0) {
                response.setSuccess(true);
            } else {
                response.setSuccess(false);
            }
        } catch (org.springframework.dao.DuplicateKeyException dke){
            LOG.error("Username,phonenumber or email already exists:"+user.getUsername(), dke);
            response.setErrorMesssage(dke.getCause().getMessage());
        } catch (Exception e){
            LOG.error("Error adding user:"+user.getUsername(), e);
            response.setErrorMesssage(e.getMessage());
        }
        return response;
    }

    private static final class UserMapper implements RowMapper<User> {

        public User mapRow(ResultSet rs, int rowNum) throws SQLException {
            User user = new User();
            user.setId(rs.getInt("id"));
            user.setUsername(rs.getString("username"));
            user.setEmailAddress(rs.getString("email"));
            user.setPhoneNumber(rs.getString("phone"));
            user.setLatitude(rs.getString("latitude"));
            user.setLongitude(rs.getString("longitude"));
            user.setUrl(rs.getString("link"));
            return user;
        }
    }
}
