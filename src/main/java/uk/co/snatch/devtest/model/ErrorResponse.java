package uk.co.snatch.devtest.model;

import io.swagger.annotations.ApiModelProperty;

import java.util.List;

public class ErrorResponse {

    public ErrorResponse(String code, String details) {
        this.code = code;
        this.details = details;
    }

    @ApiModelProperty(example = "045")
    private String code;

    @ApiModelProperty(example = "Error message")
    private String details;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }
}
