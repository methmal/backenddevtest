package uk.co.snatch.devtest.model;

import io.swagger.annotations.ApiModelProperty;

public class LocationAddResponse {

    @ApiModelProperty(required = true, example = "true")
    private boolean success;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
