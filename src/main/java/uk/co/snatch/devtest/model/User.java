package uk.co.snatch.devtest.model;

import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

public class User {

    @ApiModelProperty(hidden=true)
    private long id;

    @NotEmpty
    @ApiModelProperty(required = true, example = "mrman")
    @Size(min=3, max=12)
    private String username;

    @NotEmpty
    @ApiModelProperty(required = true, example = "mrman@gmail.com")
    private String emailAddress;

    @NotEmpty
    @ApiModelProperty(required = true, example = "07777777777")
    private String phoneNumber;

    @ApiModelProperty(example = "-15.623037")
    private String latitude;

    @ApiModelProperty(example = "18.388672")
    private String longitude;

    @ApiModelProperty(hidden=true)
    private String url;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
