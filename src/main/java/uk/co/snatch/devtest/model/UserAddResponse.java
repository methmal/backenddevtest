package uk.co.snatch.devtest.model;

public class UserAddResponse {

    public UserAddResponse() {
    }

    public UserAddResponse(boolean success) {
        this.success = success;
    }

    private long id;

    private boolean success;

    private String errorMesssage;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getErrorMesssage() {
        return errorMesssage;
    }

    public void setErrorMesssage(String errorMesssage) {
        this.errorMesssage = errorMesssage;
    }
}
