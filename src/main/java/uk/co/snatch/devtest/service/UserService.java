package uk.co.snatch.devtest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uk.co.snatch.devtest.dao.UserDao;
import uk.co.snatch.devtest.model.LocationAddResponse;
import uk.co.snatch.devtest.model.User;
import uk.co.snatch.devtest.model.UserAddResponse;

@Service
public class UserService {

    @Autowired
    UserDao userDao;

    public UserAddResponse createUser(User user){

        return userDao.addUser(user);
    }

    public User getUser(long id){
         return userDao.findById(id);
    }

    public LocationAddResponse addLocation(long userId, String latitude, String longitude){
        return userDao.addLocation(userId, latitude, longitude);
    }
}
