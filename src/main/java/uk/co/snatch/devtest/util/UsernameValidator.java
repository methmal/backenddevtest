package uk.co.snatch.devtest.util;

import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.stream.Stream;

@Component
public class UsernameValidator {

    private static final String[] catArray = {"cat", "catfish", "scatter"};
    private static final String[] dogArray = {"dog", "bulldog"};
    private static final String[] horseArray = {"horse", "seahorse"};


    public boolean isValidUsername(String userName){

        boolean isValid = true;

        if (userName.toLowerCase().contains(catArray[0])) {
            isValid = false;
            if (userName.toLowerCase().contains(catArray[1]) || userName.toLowerCase().contains(catArray[2])){
                isValid = true;
            }
        }

        if (userName.toLowerCase().contains(dogArray[0])) {
            isValid = false;
            if (userName.toLowerCase().contains(dogArray[1])){
                isValid = true;
            }
        }

        if (userName.toLowerCase().contains(horseArray[0])) {
            isValid = false;
            if (userName.toLowerCase().contains(horseArray[1])){
                isValid = true;
            }
        }

        return isValid;
    }

}
