CREATE TABLE users (
  id  INTEGER PRIMARY KEY AUTO_INCREMENT,
  username VARCHAR(30),
  email  VARCHAR(50),
  phone VARCHAR(50),
  latitude VARCHAR(50),
  longitude VARCHAR(50),
  link VARCHAR(100),
  UNIQUE (username),
  UNIQUE (email),
  UNIQUE (phone)
);