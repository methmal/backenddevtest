package uk.co.snatch.devtest.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.RestAssured;
import io.restassured.parsing.Parser;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;
import org.apache.http.HttpStatus;
import org.hamcrest.CoreMatchers;
import org.junit.runner.RunWith;
import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import uk.co.snatch.devtest.model.User;
import uk.co.snatch.devtest.model.UserAddResponse;
import uk.co.snatch.devtest.service.UserService;

import static io.restassured.RestAssured.given;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.any;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)
public class UserControllerTest {

    public static final String USER_ADD_ENDPOINT = "/user/add";
    public static final String GET_USER_ENDPOINT = "/user/{user-id}";

    @LocalServerPort
    int serverPort;

    @MockBean
    UserService mockUserService;

    @Before
    public void setUp() throws Exception {
        RestAssured.port = serverPort;
        RestAssured.defaultParser = Parser.JSON;
    }

    @Test
    public void shouldAddUser(){
        User payload = createUser();

        UserAddResponse userAddResponse = new UserAddResponse();
        userAddResponse.setSuccess(true);

        when(mockUserService.createUser(any(User.class))).thenReturn(userAddResponse);

        RequestSpecification request = given()
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .body(payload)
                .log()
                .everything();

        // When
        Response response = request.when()
                .post(USER_ADD_ENDPOINT);

        // Then
        ValidatableResponse validatableResponse = response.then()
                .log()
                .everything();

        validatableResponse.statusCode(HttpStatus.SC_OK)
                .body("success", CoreMatchers.is(CoreMatchers.equalTo(true)));

    }

    @Test
    public void shouldReturnUserById(){
        User user = createUser();

        when(mockUserService.getUser(1)).thenReturn(user);

        RequestSpecification request = given()
                .accept(MediaType.APPLICATION_JSON_VALUE)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .pathParam("user-id", "1")
                .log()
                .everything();

        // When
        Response response = request.when()
                .get(GET_USER_ENDPOINT);

        // Then
        ValidatableResponse validatableResponse = response.then()
                .log()
                .everything();

        validatableResponse.statusCode(HttpStatus.SC_OK)
                .body("username", CoreMatchers.is(CoreMatchers.equalTo("someuser")))
                .body("emailAddress", CoreMatchers.is(CoreMatchers.equalTo("someuser@gmail.com")));

    }

    private User createUser(){
        User user = new User();

        user.setUsername("someuser");
        user.setEmailAddress("someuser@gmail.com");
        user.setPhoneNumber("077777777877");
        user.setLongitude("15.14");
        user.setLongitude("15.23");
        user.setUrl("http://google.maps.com/");

        return user;
    }
}
