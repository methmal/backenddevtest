package uk.co.snatch.devtest.dao;

import org.junit.Before;
import org.junit.Test;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import uk.co.snatch.devtest.model.User;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;


public class UserDaoImplTest {

    private UserDaoImpl userDao;

    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Before
    public void setup() throws Exception {
        userDao = new UserDaoImpl();
        EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();
        EmbeddedDatabase db = builder.setType(EmbeddedDatabaseType.H2).addScript("db/sql/create-db.sql").addScript("db/sql/insert-data.sql").build();

        namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(db);
        userDao.setNamedParameterJdbcTemplate(namedParameterJdbcTemplate);

    }

    @Test
    public void shouldInsertUser(){
        assertThat(userDao.addUser(createUser()).isSuccess(), is(equalTo(true)));
    }

    @Test
    public void shouldReturnUser(){
        assertThat(userDao.findById(1).getUsername(), is(equalTo("anjana")));
    }

    @Test
    public void shouldAddLocation(){
        assertThat(userDao.addLocation(1, "15.14", "16.17").isSuccess(), is(equalTo(true)));
    }

    private User createUser(){
        User user = new User();

        user.setUsername("someuser");
        user.setEmailAddress("someuser@gmail.com");
        user.setPhoneNumber("077777777877");
        user.setLongitude("15.14");
        user.setLongitude("15.23");
        user.setUrl("http://google.maps.com/");

        return user;
    }
}
