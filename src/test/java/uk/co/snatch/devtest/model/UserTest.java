package uk.co.snatch.devtest.model;

import org.junit.Test;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;

public class UserTest {
    private Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

    @Test
    public void shouldValidateUserObject(){
        //Given
        User user = new User();

        //When
        List<String> validationMessages = validator.validate(user).stream().
                map(constraint -> constraint.getPropertyPath() + " " + constraint.getMessage()).collect(toList());

        //Then
        assertThat(validationMessages, hasSize(3));
        assertThat(validationMessages, hasItem("phoneNumber may not be empty"));
        assertThat(validationMessages, hasItem("username may not be empty"));
        assertThat(validationMessages, hasItem("emailAddress may not be empty"));

    }
}
