package uk.co.snatch.devtest.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import uk.co.snatch.devtest.dao.UserDaoImpl;
import uk.co.snatch.devtest.model.User;
import uk.co.snatch.devtest.model.UserAddResponse;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

    @Mock
    User user;

    @Mock
    UserDaoImpl userDao;

    @InjectMocks
    UserService userService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void shouldInsertUser(){
        UserAddResponse userAddResponse = new UserAddResponse();
        userAddResponse.setSuccess(true);
        when(userDao.addUser(any(User.class))).thenReturn(userAddResponse);

        assertThat(userService.createUser(new User()).isSuccess(), is(equalTo(true)));
    }

}
