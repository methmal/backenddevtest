package uk.co.snatch.devtest.util;

import org.junit.Before;
import org.junit.Test;


import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class UsernameValidatorTest {

    private UsernameValidator usernameValidator;

    @Before
    public void setup() throws Exception {
        usernameValidator = new UsernameValidator();
    }

    @Test
    public void shouldValidateUsernames(){
        assertThat(usernameValidator.isValidUsername("cat"), is(equalTo(false)));
        assertThat(usernameValidator.isValidUsername("catFish"), is(equalTo(true)));
        assertThat(usernameValidator.isValidUsername("doogCAT"), is(equalTo(false)));
        assertThat(usernameValidator.isValidUsername("doogcatfish"), is(equalTo(true)));
        assertThat(usernameValidator.isValidUsername("DOGcatfish"), is(equalTo(false)));
    }
}
